import React, {Component, Fragment} from 'react';
import logo from "../resources/img/Group 2.png";
import heart from "../resources/img/Group 63.svg";

class Menu extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Fragment>
                <header className="Header">
                    <img className="Header-logo" src={logo}></img>
                    <label className="Header-hamburger mb-0" for="menuCheck"><i class="fas fa-bars"></i></label>
                    <input autoComplete="off" type="checkbox" name="menuCheck" id="menuCheck" className="Header-hamburgerCheck d-none"></input>
                    <nav className="Header-navigation">
                        <ul className="Header-navigationList">
                            <li className="Header-navigationItem">
                                <a href="#" className="Header-link">Servicios <i class="fas fa-sort-up Header-icon"></i></a>
                                <div className="Header-subMenu">
                                    <a href="#" className="Header-linkSubmenu">Peluqueria</a>
                                    <a href="#" className="Header-linkSubmenu">Maquillaje</a>
                                    <a href="#" className="Header-linkSubmenu">Uñas</a>
                                    <a href="#" className="Header-linkSubmenu">Estética</a>
                                </div>
                            </li>
                            <li className="Header-navigationItem">
                                <a href="#" className="Header-link">Productos</a>
                                <div className="Header-subMenu">
                                    <a href="#" className="Header-linkSubmenu">Peluqueria</a>
                                    <a href="#" className="Header-linkSubmenu">Maquillaje</a>
                                    <a href="#" className="Header-linkSubmenu">Uñas</a>
                                    <a href="#" className="Header-linkSubmenu">Estética</a>
                                </div>
                            </li>
                            <li className="Header-navigationItem">
                                <a href="#" className="Header-link">Bodas</a>
                                <div className="Header-subMenu">
                                    <a href="#" className="Header-linkSubmenu">Peluqueria</a>
                                    <a href="#" className="Header-linkSubmenu">Maquillaje</a>
                                    <a href="#" className="Header-linkSubmenu">Uñas</a>
                                    <a href="#" className="Header-linkSubmenu">Estética</a>
                                </div>
                            </li>
                            <li className="Header-navigationItem">
                                <a href="#" className="Header-link">Estilos</a>
                                <div className="Header-subMenu">
                                    <a href="#" className="Header-linkSubmenu">Peluqueria</a>
                                    <a href="#" className="Header-linkSubmenu">Maquillaje</a>
                                    <a href="#" className="Header-linkSubmenu">Uñas</a>
                                    <a href="#" className="Header-linkSubmenu">Estética</a>
                                </div>
                            </li>
                            <li className="Header-navigationItem">
                                <a href="#" className="Header-link">Blog</a>
                                <div className="Header-subMenu">
                                    <a href="#" className="Header-linkSubmenu">Peluqueria</a>
                                    <a href="#" className="Header-linkSubmenu">Maquillaje</a>
                                    <a href="#" className="Header-linkSubmenu">Uñas</a>
                                    <a href="#" className="Header-linkSubmenu">Estética</a>
                                </div>
                            </li>
                            <a className="Header-action hidden"></a>
                            <a className="Header-action hidden"></a>
                        </ul>
                    </nav>

                    <div className="Header-actions">
                        <a href="#"className="Header-action">Iniciar sesión</a>
                        <a href="#"className="Header-action">Reserva</a>
                        <a href="#"className="Header-action">ES</a>
                        <a href="#"className="Header-action heart">
                            <img className="Header-heart" src={heart}></img>
                        </a>
                        <a href="#"className="Header-action"><i class="fas fa-shopping-bag iconStoreBag"></i></a>
                    </div>
                </header> 
            </Fragment>
        )
    }
}

export default Menu;