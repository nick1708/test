import React, { Component, Fragment } from 'react';
import imagen1 from "../resources/img/Group 96.svg";
import imagen2 from "../resources/img/Group 107.svg";
import imagen3 from "../resources/img/Group 111.svg";

class Services extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Fragment>
                <section className="Services">
                    <div className="container-fluid">
                        <h2 className="Services-title">Servicios</h2>
                        <p className="Services-text">Belify es la plataforma lider en España en servicios de belleza a domicilio. Unete a Belify para acceder a los mejores profesionales en peluqueria, maquillaje, uñas y estetica</p>
                        <div className="Services-wrapperRow">
                            <div className="Services-wrapper">
                                <img className="Services-image" src={imagen1}></img>
                                <p className="Services-titleService">Tu belleza</p>
                                <p className="Services-description">¿Como quieres sentirte hoy? Simplemente selecciona los servicios que se ajustan a tus necesidades de belleza y nosotros nos encargamos del resto. Cualquiera que sea la solicitud siempre adaptamos un enfoque personalizado para cada cita.</p>
                            </div>

                            <div className="Services-wrapper">
                                <img className="Services-image" src={imagen2}></img>
                                <p className="Services-titleService">Tu disponibilidad y tu ubicación   </p>
                                <p className="Services-description">Una reunion a las 8 de la mañana? Una cena a las 11 de la noche? No importa donde te encuentres o como tengas la agenda, ofrecemos una experiencia que se adapta a tu vida y a tu horario porque la belleza sucede cada segundo a cada dia.</p>

                            </div>

                            <div className="Services-wrapper mr-0">
                                <img className="Services-image" src={imagen3}></img>
                                <p className="Services-titleService">Nuestros profesionales</p>
                                <p className="Services-description">Bellify pone a tu disposicion a los profesionales que mejor se adaptan a tus necesidades, quienes llegaran al lugar indicado con los mejores productos y utensilios garantizando el mejor resultado.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        )
    }
}

export default Services;