import React, {Component, Fragment} from 'react';
import Reservation from "./reservation";
class Banner extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Fragment>
               <section className="Banner">
                <div className="container Banner-myContainer">
                    <div className="Banner-wrapper">
                        <div className="Banner-content">
                            <h1 className="Banner-title">tu asesor de belleza a domicilio</h1>
                            <p className="Banner-text">Servicios de peluquería, maquillaje, uñas, y estetica</p>
                            <p className="Banner-text">Productos imprescindibles.</p>
                            <p className="Banner-text">En cualquier lugar, en cualquier lugar momento</p>
                        </div>
                    </div>
                </div>
                <Reservation />
               </section>
            </Fragment>
        )
    }
}

export default Banner;