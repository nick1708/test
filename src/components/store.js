import React, {Component, Fragment} from 'react';

class Store extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Fragment>
                <section className="Store">
                    <div className="container">
                        <div className="Store-wrapper">
                            <h2 className="Store-title">Tienda Online</h2>
                            <p className="Store-text">Accede a los productos de belleza utilizados por los mejores profesionales</p>
                            <a href="#" className="Store-button">Comprar ahora</a>
                        </div>
                    </div>
                </section>  
            </Fragment>
        )
    }
}

export default Store;