import React, {Component, Fragment} from 'react';

class Portfolio extends Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <Fragment>
                <section className="Portfolio">
                    <div className="Portfolio-wrapper hairdresser">
                        <div className="Portfolio-internal">
                            <h3 className="Portfolio-title">Peluquería</h3>
                            <p className="Portfolio-text">Lorem ipsum dolor sit amet, consectetur adipiscingelit. Pellentesque convallis dictum tellus, in rutrun purus</p>
                            <a href="#" className="Portfolio-button">Contratar</a>
                        </div>
                    </div>
                    <div className="Portfolio-wrapper hairdresser2">
                        <div className="Portfolio-internal">
                            <h3 className="Portfolio-title">Peluquería</h3>
                            <p className="Portfolio-text">Lorem ipsum dolor sit amet, consectetur adipiscingelit. Pellentesque convallis dictum tellus, in rutrun purus</p>
                            <a href="#" className="Portfolio-button">Contratar</a>
                        </div>
                    </div>
                    <div className="Portfolio-wrapper nails">
                        <div className="Portfolio-internal">
                            <h3 className="Portfolio-title">Uñas</h3>
                            <p className="Portfolio-text">Lorem ipsum dolor sit amet, consectetur adipiscingelit. Pellentesque convallis dictum tellus, in rutrun purus</p>
                            <a href="#" className="Portfolio-button">Contratar</a>
                        </div>
                    </div>
                    <div className="Portfolio-wrapper beautyShop">
                        <div className="Portfolio-internal">
                            <h3 className="Portfolio-title">Estética</h3>
                            <p className="Portfolio-text">Lorem ipsum dolor sit amet, consectetur adipiscingelit. Pellentesque convallis dictum tellus, in rutrun purus</p>
                            <a href="#" className="Portfolio-button">Contratar</a>
                        </div>
                    </div>
                    <div className="Portfolio-wrapper groups">
                        <div className="Portfolio-internal">
                            <h3 className="Portfolio-title">Grupos</h3>
                            <p className="Portfolio-text">Lorem ipsum dolor sit amet, consectetur adipiscingelit. Pellentesque convallis dictum tellus, in rutrun purus</p>
                            <a href="#" className="Portfolio-button">Contratar</a>
                        </div>
                    </div>
                    <div className="Portfolio-wrapper package">
                        <div className="Portfolio-internal">
                            <h3 className="Portfolio-title">Paquetes</h3>
                            <p className="Portfolio-text">Lorem ipsum dolor sit amet, consectetur adipiscingelit. Pellentesque convallis dictum tellus, in rutrun purus</p>
                            <a href="#" className="Portfolio-button">Contratar</a>
                        </div>
                    </div>
                </section>  
            </Fragment>
        )
    }
}

export default Portfolio;