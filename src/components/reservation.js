import React, { Component, Fragment } from 'react';
import bannerImage1 from "../resources/img/Group 69.svg";
import bannerImage2 from "../resources/img/Path 124.svg";
import bannerImage3 from "../resources/img/Group 76.svg";
import bannerImage4 from "../resources/img/Group 95.svg";

class Reservation extends Component { 
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Fragment>
                <div className="Banner-boxWrapper">
                    <a href="#" className="Banner-item">
                        <img className="Banner-itemImage" src={bannerImage1}></img>
                        Localidad
                        <i class="fas fa-chevron-down Banner-icon"></i>
                    </a>
                    <a href="#" className="Banner-item">
                        <img className="Banner-itemImage" src={bannerImage2}></img>
                        Tratamiento
                        <i class="fas fa-chevron-down Banner-icon"></i>
                    </a>
                    <a href="#" className="Banner-item">
                        <img className="Banner-itemImage" src={bannerImage3}></img>
                        Fecha
                        <i class="fas fa-chevron-down Banner-icon"></i>
                    </a>
                    <a href="#" className="Banner-item">
                        <img className="Banner-itemImage" src={bannerImage4}></img>
                        Hora
                        <i class="fas fa-chevron-down Banner-icon"></i>
                    </a>
                    <a href="#" className="Banner-item">Reservar</a>
                </div>
            </Fragment>
        )
    }
}

export default Reservation;