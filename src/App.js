import React from 'react';
import './resources/css/app.css';
import Menu from './components/menu';
import Banner from './components/banner';
import ReservationHidden from "./components/reservationHiddden"
import Store from './components/store';
import Services from './components/services';
import Portfolio from './components/portfolio';

function App() {
  return (
    <div className="App">
        <Menu />
        <Banner />
        <ReservationHidden />
        <Store />
        <Services />
        <Portfolio />
    </div>
  );
}

export default App;
